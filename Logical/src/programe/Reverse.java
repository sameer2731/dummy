package programe;

import java.util.Scanner;

public class Reverse {
	public static void main(String[]args) {

		Scanner a = new Scanner(System.in);
		System.out.println("Enter the Number");
		int b=a.nextInt();
//		int rev=0;
//		while (b!=0){
//			rev=rev*10+b%10; //0*10+1234%10=0+4=4  4*10+123%10=40+3=43 43*10+12%10=432  4320+1=4321    
//			b=b/10; //1234/10=123 123/10=12 12/10=1 0.1=0
//		}
//		System.out.println("the Reverse Number is:- "+rev);
		
		//Reversing by StringBuffer class  
//		StringBuffer m = new StringBuffer(String.valueOf(b));
//		StringBuffer rev = m.reverse();
//		System.out.println(rev);
		
		//Reversing by StringBuilder class
		StringBuilder q = new StringBuilder();
		q.append(b);
		StringBuilder w =q.reverse();
		System.out.println(w);
		
	}

}
