package logicalpratice;

import java.util.Scanner;

public class EvenOddNumber {

	public static void main(String[]args) {
		Scanner b = new Scanner(System.in);
		System.out.println("Enter Number");
		long number=b.nextInt();
		if(number%2==0) {
			System.out.println("Number is Even");
		}
		else {
			System.out.println("Number is Odd");
		}
	}

}
