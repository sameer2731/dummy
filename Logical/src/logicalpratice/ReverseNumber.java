package logicalpratice;

import java.util.Scanner;

public class ReverseNumber {
public static void main(String[]args) {
	Scanner a = new Scanner(System.in);
	System.out.println("Enter Number");
	long b =a.nextLong();
	long rev=0;
	while(b>0) {
		rev=rev*10+b%10;
		b=b/10;
		}
	System.out.println(rev);
}

}
